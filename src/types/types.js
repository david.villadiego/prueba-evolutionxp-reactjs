/***
 * types tipos de acciones a realizar en el reducer
 * @type {login} acción realizar login
 * @type {otp} acción enviar otp
 * @type {logout} acción realizar logout
 *   ***/
export const types = {
    login: 'login',
    otp: 'otp',
    logout: 'logout'
}
