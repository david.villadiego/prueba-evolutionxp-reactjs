/***
 * WorkFlowItem componente donde visualizan lista de movimientos
 * @param {function} getFormatDate: funcion que sirve dar formato a la fecha de cada movimientos
 ***/

import React from 'react'
import {Row, Col} from "react-bootstrap";
import '../workflow-item/WorkFlowItem.css'
import imagePesos from "../../assets/images/pesos.png";
import HV from "../../assets/data/hv.pdf";
import imagePdf from "../../assets/images/pdf.png";
import {getFormatDate} from "../../helpers/getDate";


export const WorkFlowItem = ({item, flag}) => {

    const dateFormat = getFormatDate(item.fecha);

    return (
        <Row className='bg-container'>
            {flag && <>
                <Col lg={5} md={6} sm={12}
                     className='align-self-start text-center pt-2 mx-3 mb-3 flex-card-par'>
                    <div className="card card-size">
                        <div className="card-header flex-card">
                                            <span className="text-start"><img src={imagePesos} width='70px'
                                                                              alt="s"/></span>
                            <h6 className="card-subtitle mx-3 text-muted text-end">{item.operador}</h6>
                        </div>
                        <div className="card-body">
                            <p className="card-title"><b>{item.accion}</b></p>
                            <p className="card-subtitle"><b>{item.valor}</b></p>
                            <p className="card-text sizeDate"><strong>{dateFormat}</strong></p>
                            {item.doc && <section>
                                <a href={HV}>
                                    <span className=""><img className="" src={imagePdf} alt="" width='35px'/></span>
                                </a>
                            </section>}
                        </div>
                    </div>
                </Col>
                <Col lg={2} md={4} sm={12}
                     className='align-self-center text-center pt-2 flex-card sizePhone'>
                    <div className="vl text-center "></div>
                    <div><strong>{dateFormat}</strong></div>
                </Col></>
            }
            {!flag && <>
                <Col lg={{span: 2, offset: 5}} md={4} sm={12}
                     className='align-self-center text-center flex-card sizePhone'>
                    <div className="vl text-center "></div>
                    <div><strong>{dateFormat}</strong></div>
                </Col>
                <Col lg={5} md={6} sm={12} className='align-self-end text-center flex-card-inpar'>
                    <div className="card card-size">
                        <div className="card-header flex-card">
                                            <span className="text-start"><img src={imagePesos} width='70px'
                                                                              alt="s"/></span>
                            <h6 className="card-subtitle mx-3 text-muted text-end">{item.operador}</h6>
                        </div>
                        <div className="card-body">
                            <p className="card-text"><b>{item.accion}</b></p>
                            <p className="card-subtitle"><b>{item.valor}</b></p>
                            <p className="card-text sizeDate"><strong>{dateFormat}</strong></p>
                            {item.doc && <section>
                                <a href={HV}>
                                    <span className=""><img className="" src={imagePdf} alt="" width='35px'/></span>
                                </a>
                            </section>}
                        </div>
                    </div>
                </Col> </>
            }
        </Row>
    )
}
