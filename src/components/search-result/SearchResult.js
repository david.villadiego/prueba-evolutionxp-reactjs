/***
 * SearchResult componente donde visualizan lista de cuentas personales del usuario
 * @param {function} getWorkFlow: funcion que sirve activar el producto y emitir al dashboard el detalle
 * @type {Object} product: Objeto donde guarda informacion del producto bancario
 ***/

import React, {useContext, useEffect} from 'react';
import {ListGroup} from "react-bootstrap";
import {images} from "../../helpers/getImages";
import {ProductContext} from "../../hooks/useContext";


export const SearchResult = ({product}) => {

    const {setProducts} = useContext(ProductContext)
    let info = product.info;
    const getWorkFlow = (e) => {
        let elemento = document.getElementsByClassName('list-group-item');
        for (let i = 0; i < elemento.length; i++) {
            elemento[i].classList.remove('active2');
        }
        e.target.className= `${e.target.className} active2`;
        setProducts(product);
    }

    useEffect(() => {
        if (product) {
            setProducts(product)
        }
    }, [setProducts, product])

    return (
        <ListGroup as="ol" numbered onClick={getWorkFlow}>
            <ListGroup.Item
                className="d-flex justify-content-between align-items-start cursor">
                <div className="me-auto">
                    <div className="fw-bold">{info.tipo}</div>
                    {info.producto}
                </div>
                <div className="ms-2 ">
                    {info.tarjeta && <span className="text-end"> <b>{info.tarjeta} | </b></span>}
                    <span className="text-end"><img src={images[info.franquisia]} width='40px' alt="s"/></span>
                </div>
            </ListGroup.Item>
        </ListGroup>
    )
}
