/***
 * WorkFlow componente donde visualizan todos los movimientos del usuario
 * @param {function} parImpar: funcion que sirve intercambiar cada movimiento izq a dere
 ***/

import React, {useContext} from 'react';
import '../workflow/WorkFlow.css'
import {WorkFlowItem} from "../workflow-item/WorkFlowItem";
import {ProductContext} from "../../hooks/useContext";

export const WorkFlow = () => {
    const {products: workFlow} = useContext(ProductContext)

    const parImpar = (num) => {
        if (num % 2 === 0) {
            return true;
        } else {
            return false;
        }
    }
    return (
        <>
            {workFlow && <>
                <div className="container border border-3 animated fadeIn ">
                    <div className="flex-radicado">
                        <p className="text-start">Saldo Actual: <strong> {workFlow.info.saldo}</strong>, periodo
                            del: <strong>{workFlow.info.periodo}</strong></p>
                    </div>
                </div>
                <div className="search overflow-auto">
                    {
                        workFlow.movimientos.map((item, i) => {
                            return (
                                <WorkFlowItem
                                    key={i}
                                    item={item}
                                    flag={parImpar(i)}
                                />
                            );
                        })
                    }
                </div>
            </>
            } </>
    )
}



