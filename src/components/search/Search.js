/***
 * Search componente donde visualizan todos los productos del usuario
 * @param {function} handleInputChange: funcion que sirve capturar el data a buscar
 * @param {function} handleSubmit: funcion que sirve enviar el dato a buscar
 * @param {function} findProducts: funcion que sirve para buscar los productos segun lo ingresado por el usuario
 ***/

import React, {useContext, useState} from 'react';
import {Alert, Button, Container, Form, FormControl, Navbar} from "react-bootstrap";
import {SearchResult} from "../search-result/SearchResult";
import {useFetchProducts} from "../../hooks/useFetchGifs";
import imageUser from "../../assets/images/user.svg";
import {AuthContext} from "../../auth/AuthContext";

export const Search = () => {

    const {data, loading, list, setState} = useFetchProducts();
    const [inputValue, setInputValue] = useState('');
    const {user: {email}} = useContext(AuthContext)

    const handleInputChange = (e) => {
        setInputValue(e.target.value);
        findProducts();
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        findProducts();
    }

    const findProducts = () => {
        if (inputValue.trim().length > 1) {
            const result = list.products.filter(({info}) => {
                return info.tipo.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0 ||
                    info.periodo.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0 ||
                    info.producto.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0 ||
                    info.tarjeta.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0 ||
                    info.saldo.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0 ||
                    info.franquisia.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0
            })
            if (result.length > 0) {
                setState({
                    data: result,
                    list,
                    loading: false,
                    setState
                })
            } else {
                setState({
                    data: list.products,
                    list,
                    loading: false,
                    setState
                })
            }
        } else {
            setState({
                data: list.products,
                list,
                loading: false,
                setState
            })
        }
    }

    return (
        <div>
            {list &&
            (<section>
                <div className="card-header flex-card">
                                            <span className="text-start"><img src={imageUser} width='70px'
                                                                              alt="s"/></span>
                    <h6 className="card-subtitle mx-3 text-muted text-end">{email}</h6>
                </div>
            </section>)
            }
            <Navbar bg="light" expand="lg">
                <Container fluid>
                    <Form className="d-flex" onSubmit={handleSubmit}>
                        <FormControl
                            type="search"
                            placeholder="Search"
                            className="me-2"
                            aria-label="Search"
                            value={inputValue}
                            onChange={handleInputChange}
                        />
                        <Button variant="outline-success" onClick={handleInputChange}>Search</Button>
                    </Form>
                </Container>
            </Navbar>
            <div className="search overflow-auto">
                {
                    data.map(item => (
                        <SearchResult
                            key={item.id}
                            product={item}
                        />
                    ))
                }
            </div>
            {loading &&
            (<Alert variant='warning'>
                This is a alert with{' '}
                <Alert.Link href="#">an example link</Alert.Link>. Give it a click if you
                like.
            </Alert>)
            }
        </div>
    )
}
