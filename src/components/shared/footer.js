import React from 'react'


export const Footer = () => {

    return (
        <footer className="fade fadeIn text-center text-lg-start bg-light text-muted">
            <div className="text-center p-4 bg-dark">
                © 2021 Copyright: <a className="text-reset fw-bold" href="/">
                David Villadiego - davids_cadc@hotmail.com</a>
            </div>
        </footer>
    )
}
