import React, {useContext} from 'react'
import {Link, useHistory} from 'react-router-dom'
import {AuthContext} from "../../auth/AuthContext";
import {types} from "../../types/types";

export const Navbar = () => {

    const {dispatch} = useContext(AuthContext);
    const history = useHistory();

    const logout = () => {
        dispatch({
            type: types.logout,
        })
        history.replace('/login');
    }
    return (
        <nav className="fade fadeInDown navbar navbar-expand-sm navbar-dark bg-dark px-5">

            <Link
                className="navbar-brand"
                to="/">
                Sistema de información
            </Link>

            <Link
                className="navbar-brand"
                to="/">
                Home
            </Link>

            <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
                <ul className="navbar-nav ml-auto">
                    <button
                        className="nav-item nav-link btn" onClick={logout}>
                        Logout
                    </button>
                </ul>
            </div>
        </nav>
    )
}
