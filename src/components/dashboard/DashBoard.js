/***
 * Main DashBoard componente principal donde se muestra la informacion general de usuario y productos
 * @param {function} Search Componente barra lateral para listar los productos del usuario
 * @param {function} WorkFlow Componente marco donde se visualiza los movimientos de cada producto del usuario
 ***/

import React, {useState} from 'react'
import {Row, Col} from "react-bootstrap";
import {Search} from "../search/Search";
import {WorkFlow} from "../workflow/WorkFlow";
import './DashBoard.css'
import {ProductContext} from "../../hooks/useContext";
import {Footer} from "../shared/footer";

export const DashBoard = () => {

    const [products, setProducts] = useState(null);

    return (
        <ProductContext.Provider value={{products, setProducts}}>
            <>
                <div className='fade fadeIn'>
                    <Row>
                        <Col xl={4} lg={4} md={5} sm={6} className="search-panel alert-secondary">
                            <Search/>
                        </Col>
                        <Col xl={8} lg={8} md={7} sm={6}>
                            <WorkFlow/>
                        </Col>
                    </Row>
                </div>
                <Footer/>
            </>
        </ProductContext.Provider>
    )
}
