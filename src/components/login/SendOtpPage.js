/***
 * SendOtpPage componente donde emite el OTP
 * @param {function} handleClose: funcion que sirve para ocultar el modal
 * @param {function} handleShow: funcion que sirve para mostrar el modal
 * @param {function} getRandomArbitrary: funcion que genera los OTPs
 * @type {string} sendOtp: variable donde se guarda el OTP emitido
 ***/

import React, {useEffect, useState} from 'react'
import {Button, Modal} from "react-bootstrap";
import './LoginPage.css'
import otp from '../../assets/images/otp.png'

export const SendOtpPage = () => {

    const [show, setShow] = useState(false);
    let [numOtp, setNumOtp] = useState('');
    const urlMap = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6275.731009395642!2d-74.02778827162996!3d4.916186319680242!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e4077636ccb7fbb%3A0x8b398a4848784bfd!2sParque%20Principal%20-%20Cajic%C3%A1!5e0!3m2!1ses!2sco!4v1635861710714!5m2!1ses!2sco';
    const handleClose = () => setShow(false);
    const handleShow = () => {
        setShow(true)
        setNumOtp(getRandomArbitrary(100000, 999999));
    };

    const getRandomArbitrary = (min, max) => {
        let otpNum = Math.floor(Math.random() * (max - min)) + min;
        sessionStorage.setItem('sendOtp', otpNum);
        console.log(otpNum);
        return otpNum;
    }
    useEffect(() => {
        setTimeout(() => {
            handleShow();
        }, 2000)
    }, [])

    return (
        <div>
            <span className="underlineHover cursor text-otp" onClick={handleShow}>Reenviar nuevo otp</span>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <img src={otp} id="icon" width='10px' alt="User Icon"/>
                    <Modal.Title></Modal.Title>
                </Modal.Header>
                <Modal.Body className='text-center otp'>{numOtp}</Modal.Body>
                <iframe
                    src={urlMap}
                    title="ubication"
                    className='map' allowFullScreen="" loading="lazy">
                </iframe>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>
                        Cerrar
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}
