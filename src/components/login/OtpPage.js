/***
 * OtpPage componente donde se realiza la validacion del OTP enviado
 * @param {function} handleInputChange: funcion que captura el OTP del usuario
 * @param {function} validateOtp: funcion que valida si el OTP es igual al emitido
 * @param {function} sendOtp: funcion que valida el OTP del usuario y emite los nuevos cambios AuthContext
 * @type {string} type: Accion a realizar por el authReducer
 * @type {string} payload: Informacion que se le envia al authReducer OTP
 ***/

import React, {useContext, useState} from 'react'
import {useHistory} from "react-router-dom";
import {Form} from "react-bootstrap";
import './LoginPage.css'
import logo from '../../assets/images/bank1.png'
import {AuthContext} from "../../auth/AuthContext";
import {types} from "../../types/types";
import {SendOtpPage} from "./SendOtpPage";

export const OtpPage = () => {

    const {user, dispatch} = useContext(AuthContext);
    const history = useHistory();
    const [inputOtp, setinputOtp] = useState('');
    const [form, setForm] = useState({empty: false, valid: false});
    const handleInputChange = (e) => {
        setinputOtp(e.target.value);
    }

    const validateOtp = () => {
        let sendOtp = sessionStorage.getItem('sendOtp') || '';
        if (inputOtp.trim().length === 0) {
            setForm({empty: true, valid: false});
            return false;
        } else if (inputOtp.trim() !== sendOtp || inputOtp.trim().length <= 5) {
            setForm({empty: false, valid: true});
            return false;
        } else {
            setForm({empty: false, valid: false});
            return true;
        }
    }

    const sendOtp = (e) => {
        e.preventDefault();
        if (validateOtp()) {
            dispatch({
                type: types.otp,
                payload: {
                    ...user,
                    otp: inputOtp
                }
            })
            history.replace('/dashboard');
        }
    }

    return (

        <div className="wrapper fadeInDown">
            <div id="formContent">

                <div className="fadeIn first">
                    <img src={logo} id="icon" width='10px' alt="User Icon"/>
                </div>

                <Form onSubmit={sendOtp}>
                    <input type="text" id="email" className="fadeIn second" name="email"
                           placeholder="Escribe el otp enviado"
                           onChange={handleInputChange}/>
                    {form.empty && <p className='text-danger'>Debe ingresar el número otp enviado.</p>}
                    {form.valid && <p className='text-danger'>Debe ingresar un otp valido.</p>}
                    <input type="submit" className="fadeIn fourth" value="Enviar" onClick={sendOtp}/>
                </Form>
                <div id="formFooter">
                    <SendOtpPage/>
                </div>
            </div>
        </div>
    )
}
