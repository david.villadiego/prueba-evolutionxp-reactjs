/***
 * LoginPage componente donde se realiza el login del usuario validando correo electronico
 * @param {function} handleInputChange: funcion que captura el correo del usuario
 * @param {function} validateEmail: funcion que valida si el correo electronico es real
 * @param {function} login: funcion que valida el correo del usuario y emite los nuevos cambios AuthContext
 * @type {string} type: Accion a realizar por el authReducer
 * @type {string} payload: Informacion que se le envia al authReducer Email
 ***/

import React, {useContext, useState} from 'react'
import {Link, useHistory} from "react-router-dom";
import {Form} from "react-bootstrap";
import './LoginPage.css'
import logo from '../../assets/images/bank1.png'
import {AuthContext} from "../../auth/AuthContext";
import {types} from "../../types/types";
import validator from "validator/es";

export const LoginPage = () => {
    const history = useHistory();
    const {dispatch} = useContext(AuthContext);
    const [inputValue, setInputValue] = useState('');
    const [form, setForm] = useState({empty: false, valid: false});
    const handleInputChange = (e) => {
        setInputValue(e.target.value);
    }

    const validateEmail = () => {
        if (inputValue.trim().length === 0) {
            setForm({empty: true, valid: false});
            return false;
        } else if (!validator.isEmail(inputValue)) {
            setForm({empty: false, valid: true});
            return false;
        } else {
            setForm({empty: false, valid: false});
            return true;
        }
    }
    const login = (e) => {
        e.preventDefault();
        if (validateEmail()) {
            dispatch({
                type: types.login,
                payload: {
                    email: inputValue
                }
            })
            history.replace('/otp');
        }
    }


    return (
        <div className="wrapper fadeInDown">
            <div id="formContent">

                <div className="fadeIn first">
                    <img src={logo} id="icon" width='10px' alt="User Icon"/>
                </div>

                <Form onSubmit={login}>
                    <input type="text" id="email" className="fadeIn second" name="email" placeholder="example@gmail.com"
                           onChange={handleInputChange}/>
                    {form.empty && <p className='text-danger'>Correo electronico obligatorio.</p>}
                    {form.valid && <p className='text-danger'>Correo electronico invalido.</p>}
                    <input type="submit" className="fadeIn fourth" value="Log In" onClick={login}/>
                </Form>

                <div id="formFooter">
                    <Link to='/login' className="underlineHover">Forgot Password?</Link>
                </div>

            </div>
        </div>
    )
}
