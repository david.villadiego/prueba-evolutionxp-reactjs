/***
 * authReducer hook para activar acciones
 * @param {function} authReducer: hook encargado de realiazar acciones de login emitir otp y hacer logout
***/

import {types} from "../types/types";

export const authReducer = (state = {}, action) => {
    switch (action.type) {
        case types.login:
            return {
                ...action.payload,
                logged: false
            }
        case types.otp:
            return {
                ...action.payload,
                logged: true
            }
        case types.logout:
            return {logged: false}
        default:
            return state;
    }
}
