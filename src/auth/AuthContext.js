/***
 * @param {function} AuthContext: hook para manejar la información centralizada del usuario
 ***/

import {createContext} from "react";

export const AuthContext = createContext()
