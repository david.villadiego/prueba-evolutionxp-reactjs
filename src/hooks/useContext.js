/***
 * @param {function} ProductContext: hook para manejar la información centralizada de los productos
 ***/

import {createContext} from 'react'

export const ProductContext = createContext(null)
