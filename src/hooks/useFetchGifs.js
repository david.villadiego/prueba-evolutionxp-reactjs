/***
 * @param {function} useFetchProducts: hook para obtener la informacion del usuario
 ***/

import {useState, useEffect} from 'react'
import {getInfo} from '../helpers/getProducts';


export const useFetchProducts = () => {

    const [state, setState] = useState({
        data: [],
        loading: true
    });

    useEffect(() => {

        getInfo()
            .then(resp => {
                setState({
                    data: resp.products,
                    list: resp,
                    loading: false,
                    setState
                });
            })

    }, [])


    return state; // { data:[], loading: true };


}


