/***
 * @type {Object} images: tipos de imagenes
 ***/


import visa from '../assets/images/visa.png'
import banco from '../assets/images/bank.png'

export const images = {
    visa: visa,
    banco: banco
}
