/***
 * @param {function} getInfo: Servicio para obtener la información del usuario
 ***/

export const getInfo = async () => {

    const url = 'https://www.docs.qultar.com/info.json';
    const resp = await fetch(url);
    const response = await resp.json();

    return response;


}
