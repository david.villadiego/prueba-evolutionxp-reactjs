/***
 * @param {function} getFormatDate: funcion para dar formato a las fechas
 * @param {function} getFormatMonth: funcion para dar formato a los meses
 * @param {function} getFormatDay: funcion para dar formato a los dias
 ***/


export const getFormatDate = (dateItem) => {
    let date = new Date(dateItem);
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();

    const dateFormat = `${getFormatDay(day)} de ${getFormatMonth(month)} del ${year}`;
    return dateFormat;
}

function getFormatMonth(month) {
    let monthTitle = '';
    switch (month) {
        case 0:
            monthTitle = 'enero';
            break;
        case 1:
            monthTitle = 'febrero';
            break;
        case 2:
            monthTitle = 'marzo';
            break;
        case 3:
            monthTitle = 'abril';
            break;
        case 4:
            monthTitle = 'mayo';
            break;
        case 5:
            monthTitle = 'junio';
            break;
        case 6:
            monthTitle = 'julio';
            break;
        case 7:
            monthTitle = 'agosto';
            break;
        case 8:
            monthTitle = 'septiembre';
            break;
        case 9:
            monthTitle = 'octubre';
            break;
        case 10:
            monthTitle = 'noviembre';
            break;
        case 11:
            monthTitle = 'diciembre';
            break;
        default:
            break;
    }

    return monthTitle;
}

function getFormatDay(day) {
    let days = day.toString();
    if (days.length === 1) {
        days = '0' + days;
    }
    return days;
}


