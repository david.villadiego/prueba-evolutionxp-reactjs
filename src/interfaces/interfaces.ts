/***
 * @interface {Info} información del producto
 * @interface {Movimiento} movimientos del producto
 * @interface {Product} lista de productos del usuario
 * @interface {RootObject} información del ususario
 ***/

export interface Info {
  periodo: string;
  tipo: string;
  producto: string;
  tarjeta: string;
  saldo: string;
  franquisia: string;
}

export interface Movimiento {
  fecha: string;
  accion: string;
  operador: string;
  valor: string;
  doc: boolean;
}

export interface Product {
  id: string;
  info: Info;
  movimientos: Movimiento[];
}

export interface RootObject {
  correo: string;
  celular: string;
  products: Product[];
}
