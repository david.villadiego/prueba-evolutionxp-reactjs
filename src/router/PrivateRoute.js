/***
 * Private Router maneja las rutas privadas de la aplicacion como el dashboard
 * @type {boolean} isAuthenticated: variable global para identificar si un usuario esta logeado
 * @param {function} component: componente al cual se va a redireccionar el usuario
 *   ***/

import React from 'react'
import {
    Route, Redirect,
} from 'react-router-dom';
import PropTypes from 'prop-types';

export const PrivateRoute = ({isAuthenticated, component: Component, ...rest}) => {
    return (
        <Route {...rest}
               component={(props) => (
                   isAuthenticated
                       ? <Component {...props} />
                       : <Redirect to='login'/>
               )}
        />
    )
}

PrivateRoute.prototype = {
    isAuthenticated: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired
}
