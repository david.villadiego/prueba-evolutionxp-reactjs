/***
 * DashRoutes Router maneja las rutas hijas de la aplicacion como dashboard
 * @param {function} DashBoard: componente donde se carga la informacion bancaria del usuario
 ***/

import React from 'react'
import {
    Switch,
    Route,
    Redirect
} from 'react-router-dom';
import {Navbar} from "../components/shared/Navbar";
import {DashBoard} from "../components/dashboard/DashBoard";


export const DashRoutes = () => {
    return (
        <>
            <Navbar />
            <Switch>
                <Route exact path="/dashboard">
                    <DashBoard/>
                </Route>
                <Redirect to="/dashboard"  />
            </Switch>
        </>
    )
}
