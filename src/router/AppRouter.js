/***
 * Main Router maneja las 3 principales rutas login , otp y dashboard
 * @type {Object} user variable global donde se cargara la información del usuario
 ***/

import React, {useEffect, useReducer} from 'react'
import {
    BrowserRouter as Router,
    Switch,
} from 'react-router-dom';
import {LoginPage} from "../components/login/LoginPage";
import {DashRoutes} from "./DashRoutes";
import {PrivateRoute} from "./PrivateRoute";
import {AuthContext} from "../auth/AuthContext";
import {authReducer} from "../auth/authReducer";
import {PublicRoute} from "./PublicRoute";
import {OtpPage} from "../components/login/OtpPage";

export const AppRouter = () => {
    const init = () => {
        return JSON.parse(localStorage.getItem('user')) || {logged: false}
    }

    const [user, dispatch] = useReducer(authReducer, {}, init)

    useEffect(() => {
        localStorage.setItem('user', JSON.stringify(user))
    }, [user])


    console.log(user)
    return (
        <AuthContext.Provider value={{user, dispatch}}>
            <Router>
                <Switch>
                    <PublicRoute
                        exact
                        path="/login"
                        component={LoginPage}
                        isAuthenticated={user.logged}
                    />

                    <PublicRoute
                        exact
                        path="/otp"
                        component={OtpPage}
                        isAuthenticated={user.logged}
                    />

                    <PrivateRoute
                        path="/"
                        component={DashRoutes}
                        isAuthenticated={user.logged}
                    />

                </Switch>
            </Router>
        </AuthContext.Provider>
    )
}
